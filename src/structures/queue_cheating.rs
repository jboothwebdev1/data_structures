#![allow(dead_code)]

pub struct Queue<T> {
    queue: Vec<T>
}

impl <T> Queue<T> {
    pub fn new() -> Self{
        Queue { queue: vec![] }
    }

    pub fn queue(&mut self, item: T){
        self.queue.push(item); 
    }

    pub fn dequeue(&mut self) -> T {
        self.queue.remove(0)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_insert_in_last(){
        let mut queue: Queue<u32> = Queue::new();
        queue.queue(1);
        queue.queue(2);
        queue.queue(3);
        assert_eq!(queue.dequeue(), 1);
        assert_eq!(queue.dequeue(), 2);
        assert_eq!(queue.dequeue(), 3);
    }
}
