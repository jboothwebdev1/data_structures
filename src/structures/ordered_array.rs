#![allow(dead_code)]

pub trait Ordering {
    fn insert(&mut self, number: usize) -> &Self;
}

#[derive(Debug, PartialEq, Eq)]
pub struct BasicOrderedArray {
    value: Vec<usize>,
}

#[allow(dead_code)]
impl BasicOrderedArray {
    pub fn new(value: Vec<usize>) -> Self {
        BasicOrderedArray{
            value
        }
    }
} 

impl Ordering for BasicOrderedArray {
    fn insert(&mut self, number: usize) -> &Self{
        for (index, item) in self.value.clone().iter().enumerate() {
            if item > &number {
                let (left, right) = self.value.split_at(index);
                self.value = [left, &[number], right].concat()
            } else if index == self.value.len() - 1 {
                self.value.push(number)
            } 
        }
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*; 

    #[test]
    fn should_insert_before(){
        let mut testee = BasicOrderedArray::new(vec![1,2,3,5]);
        testee.insert(4);
        assert_eq!(testee.value, vec![1,2,3,4,5]);
    }

    #[test]
    fn should_insert_at_end(){
        let mut testee = BasicOrderedArray::new(vec![1,2,3,5]);
        testee.insert(6);
        assert_eq!(testee.value, vec![1,2,3,5,6]);
    }
} 
