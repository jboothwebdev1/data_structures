#![allow(dead_code)]

pub struct Stack<T> {
    stack: Vec<T>
}

impl <T> Stack<T> {
    pub fn new() -> Self {
        Stack { stack: vec![] }
    }

    pub fn insert(&mut self, item: T) {
        self.stack.insert(0, item)
    } 

    pub fn pop(&mut self) -> T {
        self.stack.remove(0)
    }
}


#[cfg(test)]

mod tests {

    use super::*;

    #[test]
    fn should_insert_and_pop_in_order(){
        let mut stack: Stack<u32> = Stack::new();
        stack.insert(3);
        stack.insert(2);
        stack.insert(1);
        assert_eq!(stack.pop(), 1);
        assert_eq!(stack.pop(), 2);
        assert_eq!(stack.pop(), 3);
    }
}
