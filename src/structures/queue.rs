#![allow(dead_code)]


#[derive(Debug)]
pub struct Queue<T: Clone + std::fmt::Debug> {
    length: usize,
    head: Option<Box<Node<T>>>
}

impl <T: Clone + std::fmt::Debug> Queue<T> {

    pub fn new() -> Self {
        Queue {
            length: 0,
            head: None
        }
    }

    pub fn queue(&mut self, value: T){
        // the current head needs to stay the head 
        // node needs to get added to the last node.
        let mut current = self.head.clone();

        let last_node = current.as_mut().map(|node| Self::find_tail_node(node));
        
        let new_node = Node::default(value);

        match last_node {
            None => self.head = Some(Box::new(new_node)),
            Some(_) => {
                last_node.unwrap().next = Some(Box::new(new_node));
                self.head = current;
            }
        };
        self.length += 1;
    }

    pub fn dequeue(&mut self) -> Result<T, &str> {
        match  &self.head {
            None => Err("No nodes in the queue."), 
            Some(_) => {
                let current = self.head.clone(); 
                let next_node = current.clone().unwrap().next;
                self.head = next_node;
                Ok(current.unwrap().value)
            }
        }
    }

   fn find_tail_node(current: &mut Box<Node<T>>)-> &mut Node<T> {
       if current.next.is_some(){
           Self::find_tail_node(current.next.as_mut().unwrap())
       } else {
            return current.as_mut()
       } 
    }

}

impl <T: Clone + std::fmt::Debug> Clone for Queue<T> {
    fn clone(&self) -> Self {
        Queue {
            length: self.length,
            head: self.head.clone()
        }
    } 
}


#[derive(Debug)]
pub struct Node<T: Clone> {
    value: T,
    next: Option<Box<Node<T>>>
}

impl<T: Clone> Node<T> {
     pub fn new(input: T, node:Box<Node<T>>) -> Self{
         Node {
             value: input,
             next: Some(node)
         }
     }  

     pub fn default(input: T) -> Self{
         Node {
             value: input,
             next: None
         }
     }
}

impl <T: Clone> Clone for Node<T> {
    fn clone(&self) -> Self {
        Node {
            value: self.value.clone(),
            next: self.next.clone()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn add_the_node(){
        let mut queue = Queue::new(); 
        queue.queue(1);
        queue.queue(2);

        assert_eq!(queue.length, 2);
    }

    #[test]
    fn should_return_in_correct_order(){
        let mut queue = Queue::new(); 
        queue.queue(1);
        queue.queue(2);
        queue.queue(3);
        assert_eq!(queue.dequeue().unwrap(), 1);
        assert_eq!(queue.dequeue().unwrap(), 2);
        assert_eq!(queue.dequeue().unwrap(), 3);
    }
}
