#![allow(dead_code)]

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct LinkedList<T: Clone + PartialEq> {
    length: u32,
    head: Option<Box<LinkedNode<T>>>
}

impl<T: PartialEq + std::clone::Clone> LinkedList<T> {
    pub fn new() -> Self {
        LinkedList { 
            length: 0,
            head: Option::None
        }
    }

    pub fn push(&mut self, value: T){

        let new_node = LinkedNode::from(value);

        let mut current = self.head.clone();

        // if self.head is equal to none then set the head to the node.
        match &mut current {
            None => self.head = Some(new_node),
            Some(next_node) => {
                //TODO: separate out to new function.
                let mut _tail_node: Option<Box<&mut LinkedNode<T>>> = None; 
                _tail_node = Some(Box::new(Self::find_empty_link(next_node)));
                _tail_node.unwrap().next = Some(new_node);
                self.head = current;
            }

       } 
        self.length += 1;
    } 

    /// Pulls the value out of the first node and returns it.  
    /// If there is a next node sets that as the head.
    pub fn unshift(&mut self) -> T {
        let current = self.head.clone();
        let next_node = current.as_ref().map(|node| node.to_owned());

        self.head = next_node;
        current.unwrap().value
    }
    
    /// Search the list for the value.
    pub fn find(&mut self, search_value: T) -> Result<T, &str> {
        let mut found = false;
        let mut current_node = &self.head;
        while !found {
           if current_node.is_some() {
                if current_node.as_ref().unwrap().value == search_value {
                    found = true;
                    return Ok(current_node.to_owned().unwrap().value)
                } else {
                    current_node = &current_node.as_ref().unwrap().next;
                }    
           } else {
               return Err("Value not found.")
           } 
        }       
        Err("not found")
    }
    
    pub fn delete(&mut self, search_value: T) -> Result<(), &str> {
        let found = false;
        // find the node with the value
        let mut head = &self.head;
        let mut new_list = head.clone();
        let mut current_node = head.clone();
        let mut previous_node: Option<Box<LinkedNode<T>>> = None; 

        if head.as_ref().unwrap().value == search_value {
            self.head = head.as_ref().unwrap().next.clone();
            return Ok(())
        } else {
            while !found {
                let mut update = LinkedNode::from(current_node.as_ref().unwrap().value.clone());

                let mut next_node = &current_node.as_ref().unwrap().next;

                if next_node.is_some() && next_node.as_ref().unwrap().value == search_value{
                    update.next = next_node.to_owned().unwrap().next.clone();  
                    current_node = Some(update);
                    if previous_node.is_some() {
                        new_list.as_mut().unwrap().next = current_node;
                    } else {
                        new_list = current_node;
                    }
                    self.length -= 1;
                    self.head = new_list;
                    return Ok(())
                } else {
                    previous_node = current_node.to_owned();
                    current_node = next_node.to_owned();
                    next_node = &current_node.as_ref().unwrap().next;  
                }
            }
            return Err("Not Found") 
        }
    }

    fn find_empty_link(current_node: &mut Box<LinkedNode<T>>) -> &mut LinkedNode<T> {
        if current_node.next.is_some(){
            Self::find_empty_link(current_node.next.as_mut().unwrap())
        } else {
            return current_node.as_mut()
        }
    }
}



/// This is nodes that will store the referances to the next nodes.
/// - *`Value`* The item to be stored in the Node.
/// - *`Next`* An option that stores a referance to the next Node or None.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct LinkedNode<T> where T: Clone + PartialEq  {
    value: T,
    next: Option<Box<LinkedNode<T>>> 
}

impl <T: PartialEq + std::clone::Clone> LinkedNode<T> {
    pub fn from(value: T) -> Box<LinkedNode<T>> {
        Box::new(LinkedNode {
            value,
            next: Option::None
        })
    }
}
    

#[cfg(test)]
mod tests{
    use super::*;
    
    #[test]
    fn should_add_node_to_list(){
        let target = LinkedList {
            length: 1,
            head: Some(Box::new(
                    LinkedNode{
                        value: 1,
                        next: None
                    }
                )
            )
        };

        let mut list = LinkedList::new();
        list.push(1);

        fn check_for_node(a: Option<Box<LinkedNode<u32>>>, b: Option<Box<LinkedNode<u32>>>) -> bool {
            a.as_ref().unwrap().value == b.as_ref().unwrap().value &&
               a.unwrap().next.is_none() && b.unwrap().next.is_none()
        }

        assert_eq!(list.length, target.length);
        assert!(check_for_node(list.head, target.head));
    }

    #[test]
    fn should_unshift_the_first_value(){
        let mut target = LinkedList {
            length: 2,
            head: Some(
                Box::new(
                    LinkedNode { value: 1, next: (
                        Some(Box::new(
                            LinkedNode { value: 2, next: None }
                            ))
                    )}
                ))
        };
        assert_eq!(target.unshift(), 1); 
    }

    #[test]
    fn should_find_the_value(){
        let mut target = LinkedList {
            length: 2,
            head: Some(
                Box::new(
                    LinkedNode { value: 1, next: (
                        Some(Box::new(
                            LinkedNode { value: 2, next: None }
                            ))
                    )}
                ))
        };

        assert_eq!(target.find(1), Ok(1)); 
        assert_eq!(target.find(2), Ok(2)); 
    }

    #[test]
    fn should_not_find_the_value(){
        let mut target = LinkedList {
            length: 2,
            head: Some(
                Box::new(
                    LinkedNode { value: 1, next: 
                        Some(Box::new(
                            LinkedNode { value: 2, next: None }
                            ))
                    }
                ))
        };

        assert_eq!(target.find(3), Err("Value not found.")); 
    }

    #[test]
    fn should_delete_the_correct_node(){
        let mut target = LinkedList {
            length: 2,
            head: Some(
                Box::new(
                    LinkedNode { value: 1, next: 
                        Some(Box::new(
                            LinkedNode { value: 2, next: 
                                Some(Box::new(
                                        LinkedNode { value: 3, next: None }
                                        ))
                            }
                            ))
                    }
                ))
        };
        target.delete(1).unwrap();
        assert_eq!(target.find(1), Err("Value not found."));
    }
}

