#![allow(dead_code)]
#[derive(Debug)]
pub struct Stack<T: Clone + std::fmt::Debug> {
    length: usize,
    head: Option<Box<Node<T>>>    
}

impl<T: Clone + std::fmt::Debug> Stack<T> {
    pub fn new() -> Self {
        Stack{
          length: 0,
          head: None 
        }
    } 

    pub fn push(&mut self, value: T) {
        // If self.head is none return none, if self.head contains a pointer we need to pull the
        // pointer out and give it to the node.
        //
        let current_head = match &self.head {
            None => None, 
            Some(_node) => self.head.clone()
        };

        let new_node = match &current_head {
            None => Self::create_initial_node(value),
            Some(node) => Self::create_continuation_node(value, node.to_owned())
        };
        
        self.head = Some(Box::new(new_node));
        self.length += 1;
    }

    pub fn pop(&mut self) -> Option<T> {
        if self.head.is_some() {
            let current_head =  self.head.clone();
            let new_head = self.head.clone().unwrap().next;
            self.head = new_head;
            self.length -= 1 ;

            Some(current_head.unwrap().value)
        } else {
            None 
        }
    }

    fn create_initial_node(value: T) -> Node<T> {
        Node::default(value) 
    }

    fn create_continuation_node(value: T, node: Box<Node<T>>) -> Node<T>{
        Node::new(value, node)
   }
} 

impl<T: Clone + std::fmt::Debug> Clone for Stack<T> {
    fn clone(&self) -> Self{
        Stack {
            length: self.length,
            head: self.head.clone()
        }
    }
}

#[derive(Debug)]
pub struct Node<T: Clone> {
    value: T,
    next: Option<Box<Node<T>>>
}

impl<T: Clone> Node<T> {

    pub fn new(input: T, node: Box<Node<T>>)  -> Self {
        Node {
            value: input,
            next: Some(node),
        } 
    }

    pub fn default(input: T) -> Self{
        Node {
            value: input,
            next: None
        }
    }
}

impl<T: Clone> Clone for Node<T> {
    fn clone(&self) -> Self{
        Node {
            value: self.value.clone(),
            next: self.next.clone()
        }
    }
}


#[cfg(test)]

mod tests {
    use super::*;

    #[test]
    fn should_insert_and_pop_in_order(){
        let mut stack = Stack::new(); 
        stack.push(1);
        stack.push(2);
        let first = stack.pop().unwrap();
        let second = stack.pop().unwrap();
        assert_eq!(first, 2);
        assert_eq!(second, 1);
    }
}
